<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

trait DelegateTableHandlerConsumer {

	/**
	 * @var DelegateTableHandler
	 */
	protected $delegateHandler;

	public function getHandler() {
		return $this->delegateHandler;
	}

	public function setHandler( $handler ) {
		$this->delegateHandler = $handler;
		return $this;
	}
}