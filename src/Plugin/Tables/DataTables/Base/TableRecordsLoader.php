<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

/**
 * @property int     $limit
 * @property int     $offset
 * @property array[] $wheres
 * @property string  $order_by
 * @property string  $order_dir
 */
class TableRecordsLoader extends \FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass {

	use DelegateTableHandlerConsumer;

	public function retrieve() :array {
		return [];
	}

	public function totalAll() :int {
		return 0;
	}

	public function totalFromWheres() :int {
		return 0;
	}

	protected function page() :int {
		return $this->limit > 0 ? (int)\round( $this->offset/$this->limit ) + 1 : 1;
	}
}