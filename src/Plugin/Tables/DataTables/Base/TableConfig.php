<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;

abstract class TableConfig extends PluginControllerConsumerBase {

	use DelegateTableHandlerConsumer;

	abstract public function getColumnsToDisplay() :array;

	abstract protected function getColumnDefs() :array;

	abstract protected function getOrderColumnSlug() :string;

	public function build() :string {
		return \json_encode( $this->buildRaw() );
	}

	public function buildRaw() :array {
		return [
			// \array_values() to ensure data of the correct format
			'columns'     => \array_values( $this->getColumnsForDisplay() ),
			'order'       => $this->getInitialOrdering(),
			'searchPanes' => $this->getSearchPanesData()
		];
	}

	/**
	 * @throws \Exception
	 */
	public function getInitialOrdering() :array {
		$thePosition = 0;
		foreach ( $this->getColumnsToDisplay() as $position => $columnDef ) {
			if ( $columnDef === $this->getOrderColumnSlug() ) {
				$thePosition = $position;
				break;
			}
		}
		return [
			[ $thePosition, $this->getOrderMethod() ]
		];
	}

	protected function getSearchPanesData() :array {
		return [
			'cascadePanes'  => false,
			'viewTotal'     => false,
			'viewCount'     => false,
			'initCollapsed' => true,
			'i18n'          => [
				'clearMessage' => __( 'Clear All Filters' ),
			]
		];
	}

	protected function getOrderMethod() :string {
		return 'asc';
	}

	/**
	 * @throws \Exception
	 */
	public function getColumnsForDisplay() :array {
		$columns = [];
		foreach ( $this->getColumnsToDisplay() as $colSlug ) {
			$col = $this->pluckColumn( $colSlug );

			if ( $col[ 'search_builder' ] ?? false ) {
				$col[ 'className' ] .= ' search_builder';
			}

			$columns[ $colSlug ] = $col;
		}
		return $columns;
	}

	/**
	 * @throws \Exception
	 */
	protected function pluckColumn( string $columnSlug ) :array {
		$col = null;
		foreach ( $this->getColumnDefs() as $slug => $columnDef ) {
			if ( $slug === $columnSlug ) {
				$col = $columnDef;
				break;
			}
		}
		if ( empty( $col ) ) {
			throw new \Exception( 'Column Definition does not exist for slug: '.$columnSlug );
		}
		return $col;
	}
}