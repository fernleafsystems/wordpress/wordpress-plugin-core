<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

use FernleafSystems\Wordpress\Services\Services;

class TableDataColumnsExtractor {

	use DelegateTableHandlerConsumer;

	/**
	 * @var array
	 */
	protected $record;

	public function forCol( string $col, array $record ) :string {
		$this->record = $record;
		return \method_exists( $this, 'col_'.$col ) ? $this->{'col_'.$col}() : (string)( $record[ $col ] ?? 'No extract for: '.$col );
	}

	protected function getColumnContent_Date( int $ts, bool $includeTimestamp = true ) :string {
		return sprintf( '%s%s',
			Services::Request()
					->carbon( true )
					->setTimestamp( $ts )
					->diffForHumans(),
			$includeTimestamp ? sprintf( '<br /><small>%s</small>',
				Services::WpGeneral()->getTimeStringForDisplay( $ts ) ) : ''
		);
	}

	protected function getUserHref( int $uid ) :string {
		$WPP = Services::WpUsers();
		$user = $WPP->getUserById( $uid );
		return empty( $user ) ? sprintf( 'Unavailable (ID:%s)', $uid ) :
			sprintf( '<a href="%s" target="_blank">%s</a>', $WPP->getAdminUrl_ProfileEdit( $user ), $user->user_login );
	}
}