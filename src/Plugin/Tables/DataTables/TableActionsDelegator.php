<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base\DelegateTableHandler;

class TableActionsDelegator extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	protected $handlers;

	public function __construct( array $handlers ) {
		$this->handlers = $handlers;
	}

	/**
	 * @throws \Exception
	 */
	public function delegate( string $tableID, string $action, array $tableData ) :array {
		return $this->getDelegate( $tableID )->run( $action, $tableData );
	}

	/**
	 * @return DelegateTableHandler|mixed
	 * @throws \Exception
	 */
	protected function getDelegate( string $tableID ) {
		$spec = $this->handlers[ $tableID ] ?? null;
		if ( empty( $spec ) ) {
			throw new \Exception( 'Unsupported table ID - no delegate provided: '.$tableID );
		}
		/** @var ?DelegateTableHandler|mixed|string $handlerClass */
		$handlerClass = $spec[ 'handler' ];
		return ( new $handlerClass( $spec ) )->setCon( $this->con() );
	}
}