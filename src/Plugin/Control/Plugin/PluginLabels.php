<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\LabelsVO;
use FernleafSystems\Wordpress\Services\Services;

class PluginLabels extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	private $labels;

	protected function run() {
		add_filter( 'all_plugins', [ $this, 'applyLabels' ] );
	}

	public function getLabels() :LabelsVO {
		$con = $this->con();

		if ( empty( $this->labels ) ) {

			$this->labels = ( new LabelsVO() )->applyFromArray( \array_merge(
				\function_exists( 'get_plugins' ) ? ( get_plugins()[ $con->base_file ] ?? [] ) : [],
				\array_map( '\stripslashes', $con->cfg->labels )
			) );

			foreach (
				[
					'icon_url_16x16',
					'icon_url_32x32',
					'icon_url_128x128',
					'url_img_pagebanner',
					'url_img_logo_small',
				] as $img
			) {
				$labelImg = $this->labels->{$img};
				if ( !empty( $labelImg ) && !Services::Data()->isValidWebUrl( $labelImg ) ) {
					$this->labels->{$img} = $con->cntnr->urls->forImage( $labelImg );
				}
			}

			$this->labels->url_secadmin_forgotten_key = 'https://shsec.io/gc';
			$this->labels->is_whitelabelled = false;
		}

		return apply_filters( $con->cntnr->prefix->hook( 'labels' ), $this->labels );
	}

	public function applyLabels( $plugins ) {
		$plugins[ $this->con()->base_file ] = $this->getLabels()->getRawData();
		return $plugins;
	}
}