<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Container;

use FernleafSystems\Utilities\Data\Adapter\DynProperties;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionsController;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\AdminNotices\AdminNoticesHandler;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets\{
	Customizer,
	Enqueue,
	Paths,
	SVGs,
	URLs
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Capabilities\Capabilities;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RestCon;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\WpCli\WpCliCon;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\{
	OptsHandler,
	OptsLookup
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\{
	PluginActivate,
	PluginDeactivate,
	PluginDelete,
	PluginLabels,
	PluginPages,
	PluginPrefix,
	PluginReset,
	PluginURLs
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons\CronHandler;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Database\DbCon;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Base\ModCon;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Render\RenderHandler;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Request\ThisRequest;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Utilities\CacheDirHandler;

/**
 * @property ActionsController   $actions
 * @property AdminNoticesHandler $admin_notices
 * @property Customizer          $assets_customizer
 * @property CacheDirHandler     $cache_dir_handler
 * @property Capabilities        $caps
 * @property CronHandler         $crons
 * @property DbCon               $db_con
 * @property Enqueue             $enqueue
 * @property OptsHandler         $opts
 * @property OptsLookup          $opts_lookup
 * @property PluginActivate      $plugin_activate
 * @property PluginDeactivate    $plugin_deactivate
 * @property PluginDelete        $plugin_delete
 * @property PluginLabels        $plugin_labels
 * @property PluginPages         $plugin_pages
 * @property PluginReset         $plugin_reset
 * @property PluginURLs          $plugin_urls
 * @property PluginPrefix        $prefix
 * @property RenderHandler       $render
 * @property RestCon             $rest
 * @property URLs                $urls
 * @property Paths               $paths
 * @property SVGs                $svgs
 * @property ThisRequest         $this_req
 * @property WpCliCon            $wpcli
 * @property ModCon[]|mixed      $modules
 */
class AptoContainer extends \League\Container\Container {

	use PluginControllerConsumer;

	use DynProperties {
		__get as __traitGet;
	}

	public function __get( string $key ) {
		switch ( $key ) {
			case 'this_req':
				$val = $this->get( $key );
				break;

			case 'admin_notices':
			case 'assets_customizer':
			case 'caps':
			case 'crons':
			case 'db_con':
			case 'enqueue':
			case 'opts':
			case 'opts_lookup':
			case 'paths':
			case 'plugin_activate':
			case 'plugin_deactivate':
			case 'plugin_delete':
			case 'plugin_labels':
			case 'plugin_pages':
			case 'plugin_reset':
			case 'plugin_urls':
			case 'render':
			case 'rest':
			case 'svgs':
			case 'urls':
			case 'wpcli':
				$val = $this->get( 'handler.'.$key );
				break;

			case 'actions':
			case 'action_router':
				$val = $this->get( 'handler.actions.router' );
				break;

			case 'prefix':
				$val = $this->get( 'handler.plugin_prefix' );
				break;

			case 'modules':
				/** @var ModCon[] $modules */
				$modules = $this->get( 'tag.mod_cons' );
				$val = [];
				foreach ( $modules as $mod ) {
					$val[ $mod->cfg->slug ] = $mod;
				}
				break;

			default:
				$val = $this->__traitGet( $key );
				break;
		}
		return $val;
	}

	public function __isset( string $key ) {
		return true;
	}
}