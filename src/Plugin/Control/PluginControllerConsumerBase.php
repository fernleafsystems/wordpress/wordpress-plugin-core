<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control;

class PluginControllerConsumerBase implements PluginControllerAwareInterface {

	use PluginControllerConsumer;
}