<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Capabilities;

class Capabilities extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	private $premiumOnly;

	public function __construct( array $premiumOnlyCaps = [] ) {
		$this->premiumOnly = $premiumOnlyCaps;
	}

	public function hasCap( string $cap ) :bool {
		return !$this->isPremiumOnlyCap( $cap )
			   || ( self::con()->isPremiumActive() && ( \in_array( $cap, $this->currentCapabilities(), true ) ) );
	}

	protected function currentCapabilities() :array {
		return [];
	}

	private function isPremiumOnlyCap( string $cap ) :bool {
		return \in_array( $cap, $this->premiumOnly );
	}
}