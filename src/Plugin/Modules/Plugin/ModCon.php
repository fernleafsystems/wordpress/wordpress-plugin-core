<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Plugin;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render;

class ModCon extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Base\ModCon {

	public function onWpInit() :void {
		$this->ctr()->render->execute();
	}

	public function onWpLoaded() :void {
		$this->ctr()->assets_customizer->execute();

		if ( is_admin() || is_network_admin() ) {
			$this->ctr()->render->addAutoRender( Render\Components\WaitSpinner::class );
		}
		if ( $this->con()->isPluginAdminPageRequest() ) {
			$this->ctr()->render->addAutoRender( Render\ToastPlaceholder::class );
		}
	}
}