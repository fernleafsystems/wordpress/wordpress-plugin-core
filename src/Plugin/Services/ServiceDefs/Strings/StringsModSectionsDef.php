<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Strings;

class StringsModSectionsDef extends StringsDefBase {

	public const ID = 'mod_sections';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsSections::class,
			],
		];
	}
}