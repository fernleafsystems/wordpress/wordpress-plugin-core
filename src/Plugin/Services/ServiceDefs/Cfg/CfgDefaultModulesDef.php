<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal;

class CfgDefaultModulesDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'default_modules_config';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enum(),
			],
		];
	}

	protected function enum() :array {
		return [
			'modules'  => [
				[
					'slug'    => 'plugin',
					'name'    => 'Plugin Core',
					'tagline' => 'Plugin Core Module.',
				],
			],
			'sections' => [
				[
					'slug'   => 'section_hidden',
					'module' => 'plugin',
					'hidden' => true
				],
				[
					'slug'        => 'section_plugin_general',
					'module'      => 'plugin',
					'title'       => 'General Settings',
					'title_short' => 'General',
				],
			],
			'options'  => [
				[
					'key'         => 'enable_plugin',
					'section'     => 'section_plugin_general',
					'type'        => 'checkbox',
					'default'     => 'Y',
					'name'        => 'Enable Plugin',
					'summary'     => 'Enable Plugin',
					'description' => 'Completely enable or disable the plugin.',
				],
			],
		];
	}
}