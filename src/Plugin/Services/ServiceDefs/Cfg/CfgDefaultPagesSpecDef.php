<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal;

class CfgDefaultPagesSpecDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'default_pages_spec';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->defaultSpec(),
			],
		];
	}

	protected function defaultSpec() :array {
		return [
			'top_navs' => [
				[
					'id'          => 'config',
					'name'        => 'Configuration',
					'subtitle'    => 'Plugin Configuration',
					'plugin_menu' => [
						'show'         => true,
						'name'         => 'Configuration',
						'img'          => 'sliders',
						'img_hover'    => 'sliders',
						'dynamic_load' => false,
					],
				],
			],
			'sub_navs' => [
				[
					'id'          => 'plugin',
					'top_nav'     => 'config',
					'name'        => 'Configuration',
					'subtitle'    => 'Config: Plugin',
					'admin_menu'  => [
						'show' => true,
						'name' => 'Configuration',
					],
					'plugin_menu' => [
						'show'         => true,
						'dynamic_load' => true,
						'name'         => 'Plugin',
					],
				],
			],
		];
	}
}