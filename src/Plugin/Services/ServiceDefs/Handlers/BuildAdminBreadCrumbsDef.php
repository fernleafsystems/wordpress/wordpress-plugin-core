<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class BuildAdminBreadCrumbsDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'build_admin_bread_crumbs';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation\BuildAdminBreadCrumbs::class,
			],
		];
	}
}