<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class ActionsRouterDef extends DefBase {

	use Traits\TraitHandlerActions;

	public const ID = 'router';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionsController::class,
			],
			'args' => [
				new ArrayArgument( $this->enum() ),
			],
		];
	}

	protected function enum() :array {
		return $this->ctr()->get( 'cfg.enum_plugin_actions' );
	}
}