<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Traits;

trait TraitCfg {

	public static function ID() :string {
		return 'cfg.'.parent::ID();
	}
}