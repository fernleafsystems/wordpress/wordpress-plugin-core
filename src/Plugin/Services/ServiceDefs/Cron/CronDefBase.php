<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cron;

abstract class CronDefBase extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\DefBase {

	public static function ID() :string {
		return 'handler.cron.'.parent::ID();
	}

	abstract public function __invoke() :array;

	public function tags() :array {
		return [
			'crons',
		];
	}
}