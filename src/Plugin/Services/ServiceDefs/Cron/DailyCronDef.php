<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cron;

class DailyCronDef extends CronDefBase {

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons\HourlyCron::class,
			],
		];
	}
}