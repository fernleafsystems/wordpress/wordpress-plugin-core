<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs;

abstract class DefBase extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public const ID = '';

	public static function ID() :string {
		return static::ID;
	}

	abstract public function __invoke() :array;

	public function tags() :array {
		return [];
	}
}