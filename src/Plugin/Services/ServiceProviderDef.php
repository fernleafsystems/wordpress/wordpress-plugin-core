<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services;

class ServiceProviderDef {

	private $defs = null;

	public function ids() :array {
		return \array_map( function ( string $def ) {
			return $def::ID();
		}, $this->enum() );
	}

	/**
	 * @return ServiceDefs\DefBase[]|string[]
	 */
	public function enum() :array {
		if ( !\is_array( $this->defs ) ) {
			$this->defs = [];
			foreach ( $this->enumCoreServices() as $def ) {
				$this->defs[ $def::ID() ] = $def;
			}
			foreach ( $this->enumCustomServices() as $def ) {
				$this->defs[ $def::ID() ] = $def;
			}
		}
		return $this->defs;
	}

	/**
	 * Order is important as some have dependencies on others
	 * @return ServiceDefs\DefBase[]|string[]
	 */
	protected function enumCoreServices() :array {
		return [
			ServiceDefs\Handlers\AssetsCustomizerDef::class,
			ServiceDefs\Handlers\AssetPathsDef::class,
			ServiceDefs\Handlers\AssetSVGsDef::class,
			ServiceDefs\Handlers\AssetURLsDef::class,

			ServiceDefs\Handlers\PluginPrefixDef::class,

			ServiceDefs\Cfg\CfgPluginActionNameDef::class,
			ServiceDefs\Cfg\CfgEnumPluginActionsDef::class,
			ServiceDefs\Cfg\CfgDefaultModulesDef::class,
			ServiceDefs\Cfg\CfgDefaultPagesSpecDef::class,
			ServiceDefs\Cfg\CfgEnumDataTableHandlersDef::class,
			ServiceDefs\Cfg\CfgEnumModulesDef::class,
			ServiceDefs\Cfg\CfgEnumRestRoutesDef::class,
			ServiceDefs\Cfg\CfgEnumWpCliCommandsDef::class,
			ServiceDefs\Cfg\CfgPageHandlersDef::class,

			ServiceDefs\Actions\ActionsCaptureAjaxDef::class,
			ServiceDefs\Actions\ActionsCapturePluginActionDef::class,
			ServiceDefs\Actions\ActionsCaptureRedirectsDef::class,
			ServiceDefs\Actions\ActionsCaptureRestApiDef::class,
			ServiceDefs\Actions\ActionsCaptureWpCliDef::class,
			ServiceDefs\Actions\ActionsDataDef::class,
			ServiceDefs\Actions\ActionsNoncesDef::class,
			ServiceDefs\Actions\ActionsRouterDef::class,

			ServiceDefs\Handlers\AdminNoticesDef::class,
			ServiceDefs\Handlers\BuildAdminBreadCrumbsDef::class,
			ServiceDefs\Handlers\CapabilitiesDef::class,
			ServiceDefs\Handlers\CronHandlerDef::class,
			ServiceDefs\Handlers\DbConDef::class,
			ServiceDefs\Handlers\EnqueueHandlerDef::class,
			ServiceDefs\Handlers\ModulesConfigsLoader::class,
			ServiceDefs\Handlers\NavMenuBuilderDef::class,
			ServiceDefs\Handlers\OptsHandlerDef::class,
			ServiceDefs\Handlers\OptsLookupDef::class,
			ServiceDefs\Handlers\PluginActivateDef::class,
			ServiceDefs\Handlers\PluginDeactivateDef::class,
			ServiceDefs\Handlers\PluginDeleteDef::class,
			ServiceDefs\Handlers\PluginLabelsDef::class,
			ServiceDefs\Handlers\PluginPagesDef::class,
			ServiceDefs\Handlers\PluginResetDef::class,
			ServiceDefs\Handlers\PluginURLsDef::class,
			ServiceDefs\Handlers\PreModulesBootCheckDef::class,
			ServiceDefs\Handlers\RenderHandlerDef::class,
			ServiceDefs\Handlers\RestConDef::class,
			ServiceDefs\Handlers\TableActionsDelegatorDef::class,
			ServiceDefs\Handlers\TextDomainDef::class,
			ServiceDefs\Handlers\UpgradesDef::class,
			ServiceDefs\Handlers\WpCliDef::class,

			ServiceDefs\Strings\StringsModOptionsDef::class,
			ServiceDefs\Strings\StringsModSectionsDef::class,
			ServiceDefs\Strings\StringsModulesDef::class,

			ServiceDefs\ThisRequestDef::class,

			ServiceDefs\Cron\DailyCronDef::class,
			ServiceDefs\Cron\HourlyCronDef::class,

			ServiceDefs\Modules\ModuleBuildDefsDef::class,
			ServiceDefs\Modules\BuildOptionsForFormDisplayDef::class,
		];
	}

	/**
	 * @return ServiceDefs\DefBase[]|string[]
	 */
	protected function enumCustomServices() :array {
		return [];
	}
}