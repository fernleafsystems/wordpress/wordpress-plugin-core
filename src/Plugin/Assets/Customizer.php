<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Services\Services;

class Customizer extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	private $hook = '';

	private $handles = [];

	private $componentBuilders;

	public function __construct( array $componentBuilders ) {
		$this->componentBuilders = $componentBuilders;
	}

	protected function canRun() :bool {
		return !Services::WpGeneral()->isAjax();
	}

	protected function run() {
		add_filter(
			$this->ctr()->prefix->hook( 'custom_localisations' ),
			function ( array $locals, string $hook = '', array $handles = [] ) {
				$this->hook = $hook;
				$this->handles = $handles;
				return \array_merge( $locals, \array_filter( $this->buildForComponents() ) );
			},
			10, 3
		);
	}

	private function buildForComponents() :array {
		$data = [];

		$allComponents = $this->components();
		foreach ( $this->handles ?? [] as $handle ) {
			$components = \array_filter(
				$allComponents,
				function ( array $comp ) use ( $handle ) {
					return \in_array( $handle, $comp[ 'handles' ] ) && ( !isset( $comp[ 'required' ] ) || $comp[ 'required' ] );
				}
			);

			if ( !empty( $components ) ) {
				if ( empty( $data[ $handle ] ) ) {
					$data[ $handle ] = [
						$handle,
						$this->ctr()->prefix->_( 'vars_'.$handle ),
						[
							'strings' => [
								'select_action'   => __( 'Please select an action to perform.', 'wp-simple-firewall' ),
								'are_you_sure'    => __( 'Are you sure?', 'wp-simple-firewall' ),
								'absolutely_sure' => __( 'Are you absolutely sure?', 'wp-simple-firewall' ),
							],
							'comps'   => \array_map( function ( array $comp ) {
								return \is_callable( $comp[ 'data' ] ?? null ) ? \call_user_func( $comp[ 'data' ] ) : $comp[ 'data' ];
							}, $components ),
						]
					];
				}
			}
		}

		return $data;
	}

	private function components() :array {
		return apply_filters(
			$this->ctr()->prefix->hook( 'custom_localisations/components' ),
			$this->componentBuilders,
			$this->hook,
			$this->handles
		);
	}
}