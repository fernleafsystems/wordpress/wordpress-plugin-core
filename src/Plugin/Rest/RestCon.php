<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest;

use FernleafSystems\Utilities\Logic\ExecOnce;

class RestCon extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	protected $roots;

	public function __construct( array $roots ) {
		$this->roots = $roots;
	}

	protected function run() {
		$this->registerRoutes();
	}

	protected function registerRoutes() :void {
		$this->defineRoutesAsServices();
		/** @var Route\RouteBase $route */
		foreach ( $this->ctr()->get( 'tag.rest_routes' ) as $route ) {
			$route->register_routes();
		}
	}

	protected function defineRoutesAsServices() :void {
		$ctr = $this->ctr();
		foreach ( $this->roots as $restRoute ) {
			if ( \class_exists( $restRoute ) ) {
				/** TODO: pass-in route-specific configuration as argument. */
				$ctr->addShared( $restRoute )
					->addTag( 'tag.rest_routes' );
			}
		}
	}
}