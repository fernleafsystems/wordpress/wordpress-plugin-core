<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config;

use FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Opts\{
	ChangedOptsTriggers,
	PreSetOptSanitizer,
	PreStore
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\{
	PluginControllerAwareInterface,
	PluginControllerConsumer
};
use FernleafSystems\Wordpress\Services\Services;

/**
 * @property array[] $mod_opts_all
 */
class OptsHandler extends DynPropertiesClass implements PluginControllerAwareInterface {

	use PluginControllerConsumer;

	public const TYPE_ALL = 'all';
	public const TYPE_FREE = 'free';
	public const TYPE_PRO = 'pro';

	private $changes = [];

	private $values = null;

	private $merged = false;

	private $startedAsPremium = false;

	private $preStore;

	private $preSetOptSanitizer;

	private $changedOptsTriggers;

	public function __construct(
		?PreStore $preStore = null,
		?PreSetOptSanitizer $preSetOptSanitizer = null,
		?ChangedOptsTriggers $changedOptsTriggers = null
	) {
		$this->preStore = $preStore;
		$this->preSetOptSanitizer = $preSetOptSanitizer;
		$this->changedOptsTriggers = $changedOptsTriggers;
	}

	public function __get( string $key ) {
		$val = parent::__get( $key );

		if ( $val === null && $key === 'mod_opts_all' ) {

			if ( $this->con()->plugin_reset ) {
				$val = $this->defaultAllStorageStruct();
			}
			else {
				$val = Services::WpGeneral()->getOption( $this->key( self::TYPE_ALL ) );
				if ( !\is_array( $val ) ) {
					$val = $this->defaultAllStorageStruct();
				}
			}

			$this->{$key} = $val;
		}

		return $val;
	}

	public function values() :array {

		if ( $this->values === null ) {
			$all = $this->mod_opts_all;
			$this->values = $all[ 'values' ][ self::TYPE_FREE ] ?? [];
		}

		if ( !$this->merged ) {
			$this->merged = true;
			if ( $this->con()->isPremiumActive() ) {
				$this->startedAsPremium = true;
				$this->values = \array_merge( $this->values, $this->mod_opts_all[ 'values' ][ self::TYPE_PRO ] );
			}
			$this->values = \array_intersect_key( $this->values, $this->con()->cfg->configuration->options );
		}

		return $this->values;
	}

	public function resetToDefaults() {
		$this->mod_opts_all = $this->defaultAllStorageStruct();
		$this->delete();
	}

	private function key( string $type ) :string {
		return $this->ctr()->prefix->_( sprintf( 'opts_%s', $type ) );
	}

	public function delete() :void {
		foreach ( [ self::TYPE_ALL, self::TYPE_PRO, self::TYPE_FREE ] as $type ) {
			Services::WpGeneral()->deleteOption( $this->key( $type ) );
		}
	}

	public function store() {
		$con = $this->con();
		if ( !$con->plugin_deleting ) {
			add_filter( $con->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), '__return_true', 1000 );
			$this->preStore();

			Services::WpGeneral()->updateOption( $this->key( self::TYPE_ALL ), $this->mod_opts_all );

			$this->postStore();
			remove_filter( $con->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), '__return_true', 1000 );
		}
	}

	private function preStore() {
		$con = $this->con();

		// Pre-process options.
		if ( $this->preStore instanceof PreStore ) {
			$this->preStore->run();
		}

		do_action( $con->cntnr->prefix->hook( 'pre_options_store' ) );

		if ( !empty( $this->changes ) && $this->changedOptsTriggers instanceof ChangedOptsTriggers ) {
			$this->changedOptsTriggers->run( $this->changes );
		}

		$this->transposeValuesToStore();

		do_action( $con->cntnr->prefix->hook( 'after_pre_options_store' ), !empty( $this->changes ) );
	}

	private function transposeValuesToStore() :void {

		$freeValues = $this->mod_opts_all[ 'values' ][ self::TYPE_FREE ];
		foreach ( $this->values() as $optKey => $optValue ) {
			// if it's a premium option, set it to default on the free set.
			$freeValues[ $optKey ] = ( $this->optDef( $optKey )[ 'premium' ] ?? false ) ?
				$this->optDefault( $optKey ) : $optValue;
			if ( \is_array( $freeValues[ $optKey ] ) ) {
				\ksort( $freeValues[ $optKey ] );
			}
		}
		\ksort( $freeValues );

		$proValues = [];
		foreach (
			( $this->con()
				   ->isPremiumActive() && $this->startedAsPremium ) ? $this->values() : $this->mod_opts_all[ 'values' ][ self::TYPE_PRO ] as $optKey => $optValue
		) {
			$store = false;

			// Special case: These values can vary depending on whether free/pro
			if ( \in_array( $optKey, [ 'audit_trail_auto_clean', 'auto_clean' ] ) ) {
				$store = true;
			}
			elseif ( !isset( $freeValues[ $optKey ] ) ) {
				$freeValues[ $optKey ] = $optValue;
			}
			elseif ( \is_scalar( $optValue ) ) {
				if ( $optValue !== $freeValues[ $optKey ] ) {
					$store = true;
				}
			}
			elseif ( \is_array( $optValue ) ) {
				\ksort( $optValue );
				if ( \serialize( $optValue ) !== \serialize( $freeValues[ $optKey ] ) ) {
					$store = true;
				}
			}
			else {
				$store = true;
			}

			if ( $store ) {
				$proValues[ $optKey ] = $optValue;
			}
		}
		\ksort( $proValues );

		$all[ 'values' ][ self::TYPE_FREE ] = $freeValues;
		$all[ 'values' ][ self::TYPE_PRO ] = $proValues;

		$this->mod_opts_all = $all;
	}

	private function postStore() {
		$this->changes = [];
	}

	public function hasChanges() :bool {
		return !empty( $this->changes );
	}

	public function optCap( string $key ) :?string {
		return $this->optDef( $key )[ 'cap' ] ?? null;
	}

	public function optChanged( string $key ) :bool {
		return isset( $this->changes[ $key ] );
	}

	/**
	 * Use only when you're sure the option key exists.
	 */
	public function optDef( string $key ) :array {
		return $this->con()->cfg->configuration->options[ $key ];
	}

	public function optDefault( string $key ) {
		return $this->optDef( $key )[ 'default' ];
	}

	public function optEnforceValueType( string $key, $value ) {
		switch ( $this->optType( $key ) ) {
			case 'boolean':
				$value = (bool)$value;
				break;
			case 'integer':
				$value = (int)$value;
				break;
			case 'email':
			case 'password':
			case 'text':
				$value = (string)$value;
				break;
			case 'array':
			case 'multiple_select':
				if ( !\is_array( $value ) ) {
					$value = (array)$value;
				}
				break;
			default:
				break;
		}
		return $value;
	}

	public function optExists( string $key ) :bool {
		return isset( $this->con()->cfg->configuration->options[ $key ] );
	}

	public function optGet( string $key ) {
		$value = $this->values()[ $key ] ?? null;

		if ( $this->optExists( $key ) ) {
			$con = $this->con();

			$cap = $this->optCap( $key );
			if ( $value === null
				 || !$this->optIsValueTypeValid( $key, $value )
				 || ( !empty( $cap ) && !$this->ctr()->caps->hasCap( $cap ) )
				 || ( empty( $cap ) && ( $this->optDef( $key )[ 'premium' ] ?? false ) && !$con->isPremiumActive() )
			) {
				$this->optReset( $key );
				$value = $this->optDefault( $key );
			}

			$value = $this->optEnforceValueType( $key, $value );
		}

		return $value;
	}

	/**
	 * @param mixed $value
	 */
	public function optIs( string $key, $value ) :bool {
		return $this->optGet( $key ) == $value;
	}

	public function optReset( string $key ) :void {
		$this->optSet( $key, $this->optDefault( $key ) );
	}

	public function optSet( string $key, $newValue ) :self {
		try {
			/** Don't use optGet() */
			$current = $this->values[ $key ] ?? null;

			if ( $this->preSetOptSanitizer instanceof PreSetOptSanitizer ) {
				$this->preSetOptSanitizer->run( $key, $newValue );
			}

			// Here we try to ensure that values that are repeatedly changed properly reflect their changed
			// states, as they may be reverted to their original state and we "think" it's been changed.
			$valueIsDifferent = \serialize( $current ) !== \serialize( $newValue );
			// basically if we're actually resetting back to the original value
			$isResetToOriginal = $valueIsDifferent
								 && isset( $this->changes[ $key ] )
								 && ( \serialize( $this->changes[ $key ] ) === \serialize( $newValue ) );

			if ( $valueIsDifferent ) {
				if ( !isset( $this->changes[ $key ] ) ) {
					$this->changes[ $key ] = $current;
				}
				$this->values[ $key ] = $newValue;
			}

			if ( $isResetToOriginal ) {
				unset( $this->changes[ $key ] );
			}
		}
		catch ( \Exception $e ) {
			// new value was invalid and will not be stored.
		}
		return $this;
	}

	public function optType( string $key ) :string {
		return $this->optDef( $key )[ 'type' ];
	}

	private function optIsValueTypeValid( string $key, $value ) :bool {
		switch ( $this->optType( $key ) ) {
			case 'array':
			case 'multiple_select':
				$valid = \is_array( $value );
				break;
			case 'integer':
				$valid = \is_numeric( $value );
				break;
			default:
				$valid = true;
				break;
		}
		return $valid;
	}

	private function defaultAllStorageStruct() :array {
		return [
			'version' => $this->con()->cfg->version(),
			'values'  => [
				self::TYPE_FREE => [],
				self::TYPE_PRO  => [],
			],
		];
	}
}