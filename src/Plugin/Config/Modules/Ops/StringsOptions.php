<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

class StringsOptions extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	/**
	 * @return array{name:string, summary:string, description:array}
	 */
	public function getFor( string $key ) :array {
		$def = $this->ctr()->opts->optDef( $key );
		return [
			'name'       => __( $def[ 'name' ] ?? 'No Title', $this->con()->cfg->properties[ 'language' ] ?? '' ),
			'summary' => __( $def[ 'summary' ] ?? 'No Title', $this->con()->cfg->properties[ 'language' ] ?? '' ),
			'description'     => $def[ 'description' ] ?? [],
		];
	}
}