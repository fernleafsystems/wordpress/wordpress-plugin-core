<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

class SectionNotices extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function critical( string $section ) :array {
		return [];
	}

	public function notices( string $section ) :array {
		return [];
	}

	public function warnings( string $section ) :array {
		return [];
	}
}