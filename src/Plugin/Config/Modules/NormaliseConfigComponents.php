<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules;

class NormaliseConfigComponents {

	public function indexModules( array $modules ) :array {
		$indexed = [];
		foreach ( $modules as $key => $module ) {
			$indexed[ \is_numeric( $key ) ? $module[ 'slug' ] : $key ] = $module;
		}
		return $indexed;
	}

	public function indexSections( array $sections ) :array {
		$indexed = [];
		foreach ( $sections as $key => $section ) {
			$indexed[ \is_numeric( $key ) ? $section[ 'slug' ] : $key ] = $section;
		}
		return $indexed;
	}

	public function indexOptions( array $options ) :array {
		$indexed = [];
		foreach ( $options as $key => $option ) {
			if ( empty( $option[ 'section' ] ) ) {
				$option[ 'section' ] = 'section_hidden';
			}
			$option[ 'transferable' ] = $option[ 'transferable' ] ?? ( $option[ 'section' ] !== 'section_hidden' );

			$indexed[ \is_numeric( $key ) ? $option[ 'key' ] : $key ] = $option;
		}
		return $indexed;
	}
}