<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Traits;

trait CommonWpHookCallbacks {

	public function onWpInit() :void {
	}

	public function onWpLoaded() :void {
	}

	public function onWP() :void {
	}

	public function onWpAdminInit() :void {
	}

	public function onWpShutdown() :void {
	}
}