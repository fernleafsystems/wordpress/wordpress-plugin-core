<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Updates;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Services\Services;

class HandleUpgrades extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	protected function canRun() :bool {
		$previous = $this->con()->cfg->previous_version;
		return !empty( $previous );
	}

	protected function run() {
		$con = $this->con();
		$prev = $con->cfg->previous_version;

		$hook = $con->cntnr->prefix->pfx( 'plugin-upgrade' );
		if ( \version_compare( $prev, $con->cfg->version(), '<' ) && !wp_next_scheduled( $hook, [ $prev ] ) ) {
//		TODO:	$con->getModule_Plugin()->deleteAllPluginCrons();
			wp_schedule_single_event( Services::Request()->ts() + 1, $hook, [ $prev ] );
		}

		add_action( $hook, function ( $previousVersion ) {
			Services::ServiceProviders()->clearProviders();
			foreach ( $this->ctr()->modules as $mod ) {
				$H = \method_exists( $mod, 'getUpgradeHandler' ) ? $mod->getUpgradeHandler() : null;
				if ( !empty( $H ) ) {
					$H->setPrevious( $previousVersion )->execute();
				}
			}
		} );

		$con->cfg->previous_version = $con->cfg->version();
	}
}