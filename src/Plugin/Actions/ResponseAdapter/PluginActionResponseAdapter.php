<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ResponseAdapter;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionResponse;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;

class PluginActionResponseAdapter extends BaseAdapter {

	use PluginControllerConsumer;

	public function adapt( ActionResponse $response ) {
		switch ( $response->action_data[ 'notification_type' ] ?? '' ) {
			case 'wp_admin_notice':
				if ( \is_string( $response->action_response_data[ 'message' ] ?? null ) ) {
					$this->ctr()->admin_notices->addFlash(
						$response->action_response_data[ 'message' ],
						null,
						!( $response->action_response_data[ 'success' ] ?? true )
					);
				}
				break;
			default:
				break;
		}
	}
}