<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ResponseAdapter;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionResponse;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages\PagePluginAdminRestricted;

class AjaxResponseAdapter extends BaseAdapter {

	public function adapt( ActionResponse $response ) {
		$responseData = \array_merge(
			[
				'success'    => false,
				'message'    => $response->message ?? '',
				'error'      => $response->error ?? '',
				'html'       => '-',
				'page_title' => '-',
				'page_url'   => '-',
				'show_toast' => true,
			],
			$response->getRawData(),
			\is_array( $response->action_response_data ) ? $response->action_response_data : []
		);

		/**
		 * Special case where the AJAX triggers a render of the security restricted page.
		 * This approach is a bit of a hack. Is there a better way to standardise moving render data to ajax?
		 */
		if ( $response->action_slug === PagePluginAdminRestricted::SLUG ) {
			$responseData = [
				'html' => $response->action_response_data[ 'render_output' ]
			];
		}

		$response->action_response_data = $responseData;
	}
}