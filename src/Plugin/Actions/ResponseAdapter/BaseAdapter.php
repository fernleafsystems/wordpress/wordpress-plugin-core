<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ResponseAdapter;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionResponse;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions\ActionException;

class BaseAdapter {

	/**
	 * @throws ActionException
	 */
	public function adapt( ActionResponse $response ) {
	}
}