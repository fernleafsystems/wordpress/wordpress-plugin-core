<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

use FernleafSystems\Wordpress\Services\Services;

class CaptureRedirects extends CaptureActionBase {

	/**
	 * @var callable[]
	 */
	private $redirectRules;

	public function __construct( array $redirectRules = [] ) {
		$this->redirectRules = $redirectRules;
	}

	protected function canRun() :bool {
		return is_admin() && !Services::WpGeneral()->isAjax();
	}

	protected function run() {
		$redirectTo = null;
		foreach ( $this->redirectRules as $maybeRedirectCallable ) {
			$redirectTo = $maybeRedirectCallable();
			if ( !empty( $redirectTo ) ) {
				break;
			}
		}
		if ( !empty( $redirectTo ) ) {
			Services::Response()->redirect( $redirectTo, [], true, false );
		}
	}
}