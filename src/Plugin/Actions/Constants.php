<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions;

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions;

class Constants {

	public const ACTION_OVERRIDE_IS_NONCE_VERIFY_REQUIRED = 'is_nonce_verify_required';
}