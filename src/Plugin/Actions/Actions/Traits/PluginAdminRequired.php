<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

trait PluginAdminRequired {

	protected function isPluginAdminRequired() :bool {
		return true;
	}
}