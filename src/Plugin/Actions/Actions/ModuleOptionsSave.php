<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\HandleOptionsSaveRequest;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Request\FormParams;

class ModuleOptionsSave extends BaseAction {

	public const SLUG = 'mod_options_save';

	protected function exec() {
		$con = $this->con();

		$success = ( new HandleOptionsSaveRequest( FormParams::Retrieve( $this->action_data ) ) )
			->setCon( $con )
			->handleSave();

		$this->response()->action_response_data = [
			'success'     => $success,
			'html'        => '',
			'page_reload' => true,
			'message'     => $success ?
				sprintf( __( '%s Plugin options updated successfully.', 'wp-simple-firewall' ), $con->labels->Name )
				: sprintf( __( 'Failed to update %s plugin options.', 'wp-simple-firewall' ), $con->labels->Name )
		];
	}
}