<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\DynamicLoad;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages\PageConfig;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsModules;

class Config extends Base {

	public const SLUG = 'dynamic_load_config';

	protected function getPageUrl() :string {
		return $this->ctr()->plugin_urls->modCfg( $this->action_data[ 'mod_slug' ] );
	}

	protected function getPageTitle() :string {
		/** @var StringsModules $stringsModules */
		$stringsModules = $this->ctr()->get( 'handler.strings.modules' );
		return sprintf( '%s > %s',
			__( 'Configuration', 'wp-simple-firewall' ),
			$stringsModules->getFor( $this->action_data[ 'mod_slug' ] )[ 'name' ]
		);
	}

	protected function getContent() :string {
		return $this->ctr()->actions->render( PageConfig::class, $this->action_data );
	}

	protected function getRequiredDataKeys() :array {
		return [
			'mod_slug',
		];
	}
}