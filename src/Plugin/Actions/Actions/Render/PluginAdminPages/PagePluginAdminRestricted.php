<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminNotRequired;
use FernleafSystems\Wordpress\Services\Utilities\Obfuscate;

class PagePluginAdminRestricted extends BasePluginAdminPage {

	use PluginAdminNotRequired;

	public const SLUG = 'admin_plugin_page_plugin_admin_restricted';
	public const TEMPLATE = '/wpadmin/plugin_admin/inner_page/plugin_admin.twig';

	protected function getPageContextualHrefs() :array {
		return [
			[
				'text' => __( 'Disable Plugin Admin', 'wp-simple-firewall' ),
				'href' => '#',
				'id'   => 'SecAdminRemoveConfirmEmail',
			],
		];
	}

	protected function getRenderData() :array {
		$con = self::con();
		return [
			'flags'   => [
				'allow_email_override' => $con->cntnr->opts->optIs( 'allow_email_override', 'Y' )
			],
			'imgs'    => [
				'inner_page_title_icon' => $con->cntnr->svgs->raw( 'person-badge' ),
			],
			'strings' => [
				'inner_page_title'    => __( 'Security Plugin Protection', 'wp-simple-firewall' ),
				'inner_page_subtitle' => sprintf( __( 'Access to the %s Security plugin is restricted.', 'wp-simple-firewall' ),
					$con->labels->Name ),

				'force_remove_email' => __( "If you've forgotten your PIN, use the menu above to disable this restriction.", 'wp-simple-firewall' ),
				'send_to_email'      => sprintf( __( 'Confirmation email will be sent to %s', 'wp-simple-firewall' ),
					Obfuscate::Email( $con->cntnr->opts_lookup->getReportEmail() ) ),
				'no_email_override'  => __( "The Plugin Administrator has restricted the use of the email override feature.", 'wp-simple-firewall' ),
			],
		];
	}
}