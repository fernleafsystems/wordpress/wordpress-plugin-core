<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions\ActionException;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation\BuildAdminBreadCrumbs;

abstract class BasePluginAdminPage extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\BaseRender {

	protected function getPageContextualHrefs() :array {
		return [];
	}

	protected function getPageContextualHrefs_Help() :array {
		return [];
	}

	protected function getInnerPageTitle() :string {
		return '';
	}

	protected function getInnerPageSubTitle() :string {
		return '';
	}

	/**
	 * @throws ActionException
	 */
	protected function getAllRenderDataArrays() :array {
		$data = parent::getAllRenderDataArrays();
		$data[ 25 ] = $this->getCommonAdminPageRenderData();
		return $data;
	}

	protected function getCommonAdminPageRenderData() :array {
		$hrefs = $this->getPageContextualHrefs();
		$hrefs[] = $this->getPageContextualHrefs_Help();
		return [
			'hrefs' => [
				'breadcrumbs'                 => $this->getBreadCrumbs(),
				'inner_page_contextual_hrefs' => \array_filter( $hrefs ),
			],
		];
	}

	protected function getBreadCrumbs() :array {
		/** @var BuildAdminBreadCrumbs $builder */
		$builder = $this->ctr()->get( 'handler.build_admin_bread_crumbs' );
		return $builder->current();
	}
}