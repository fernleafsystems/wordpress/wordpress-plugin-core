<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\AuthNotRequired;

class ToastPlaceholder extends BaseRender {

	use AuthNotRequired;

	public const SLUG = 'render_toast_placeholder';
	public const TEMPLATE = '/components/html/bs_toaster.twig';

	protected function getRenderData() :array {
		return [
			'strings' => [
				'title' => $this->con()->labels->Name,
			],
		];
	}
}