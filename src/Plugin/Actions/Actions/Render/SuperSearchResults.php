<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminRequired;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Components\Search\SelectSearchData;

class SuperSearchResults extends BaseRender {

	use PluginAdminRequired;

	public const SLUG = 'render_super_search_results';
	public const TEMPLATE = '/wpadmin/components/search/super_search_results.twig';

	protected function getRenderData() :array {
		$results = ( new SelectSearchData() )->build( $this->action_data[ 'search' ] );
		return [
			'flags'   => [
				'has_results' => !empty( $results ),
			],
			'strings' => [
				'no_results' => __( 'Sorry, there were no results for this search.' ),
			],
			'vars'    => [
				'results' => $results,
			],
		];
	}
}