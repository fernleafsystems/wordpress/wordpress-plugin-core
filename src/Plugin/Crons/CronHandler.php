<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons;

use FernleafSystems\Utilities\Logic\ExecOnce;

class CronHandler extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	protected function run() {
		foreach ( $this->ctr()->get( 'tag.crons' ) as $cron ) {
			/** @var BaseCron $cron */
			$cron->execute();
		}
	}
}