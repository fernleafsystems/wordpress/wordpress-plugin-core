<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons;

use FernleafSystems\Wordpress\Services\Services;

/**
 * Wherever used, it must be combined with PluginControllerConsumer to access $this->con()
 */
trait PluginCronsConsumer {

	public function runDailyCron() {
	}

	public function runHourlyCron() {
	}

	protected function setupCronHooks() {
		if ( Services::WpGeneral()->isCron() ) {
			add_action( $this->ctr()->prefix->hook( 'daily_cron' ), [ $this, 'runDailyCron' ] );
			add_action( $this->ctr()->prefix->hook( 'hourly_cron' ), [ $this, 'runHourlyCron' ] );
		}
	}
}