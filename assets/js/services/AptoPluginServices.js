import { ToasterService } from "../components/toast/ToasterService";
import { BaseService } from "./BaseService";

export class AptoPluginServices extends BaseService {

	notification() {
		return new ToasterService();
	}

	container_AptoPage() {
		return document.getElementById( 'PageContainer-Apto' ) || false;
	}
}