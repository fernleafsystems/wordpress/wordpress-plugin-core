import 'js-loading-overlay';

export class AptoOverlay {

	hide() {
		AptoOverlay.Hide();
	}

	show() {
		AptoOverlay.Show();
	}

	static Hide() {
		JsLoadingOverlay.hide();
	}

	/**
	 * https://js-loading-overlay.muhdfaiz.com/
	 */
	static Show( containerID = null ) {
		let theID = null;
		if ( containerID && containerID.length > 0 && document.getElementById( containerID ) ) {
			theID = containerID;
		}
		else {
			const AptoPageContainer = document.getElementById( 'PageContainer-Apto' ) || false;
			if ( AptoPageContainer ) {
				theID = AptoPageContainer.id;
			}
		}

		JsLoadingOverlay.show( {
			"overlayBackgroundColor": "#ffffff",
			"overlayOpacity": "0.7",
			"spinnerIcon": "ball-spin-clockwise-fade",
			"spinnerColor": "#008000",
			"spinnerSize": "2x",
			"overlayIDName": "AptoOverlay",
			"spinnerIDName": "AptoOverlaySpinner",
			"offsetX": 0,
			"offsetY": "-35%",
			"containerID": theID,
			"lockScroll": false,
			"overlayZIndex": 100001,
			"spinnerZIndex": 100002
		} );
	}
}