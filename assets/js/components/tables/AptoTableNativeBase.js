import 'datatables.net-dt';
import 'datatables.net-buttons-dt';
import 'datatables.net-searchpanes-dt';
import 'datatables.net-select-dt';
import { AptoTableBase } from "./AptoTableBase";

export class AptoTableNativeBase extends AptoTableBase {
}