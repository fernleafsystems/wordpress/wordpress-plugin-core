import { AppBase } from "./AppBase";
import { ToastifyService } from "../components/toast/ToastifyService";

export class AppMainBase extends AppBase {

	notification() {
		return new ToastifyService();
	}
}