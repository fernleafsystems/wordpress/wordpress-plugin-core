#### 2.4

- [0]  Support for automating INDEX creation.

#### 2.2

- [0]  Support for RAND() ordering.

#### 2.1

- [0]  Remove Monolog and restore it in a package higher up the chain.

#### 2.0

- [0]  Move to minimum PHP 7.2.
- [1]  Upgrade Monolog from 1.x to 2.x.

#### 1.1

- [0]  rework DB handler init and deprecate all the unnecessary junk.
- [1]  fix record Iterator not actually returning the records in the query.

#### 1.0

- RTM
- [0.1]  built in verification of permissions for certain scenarios.
- [0.2]  add ability to restrict publish of routes on certain hostnames.
- [0.3]  add better drawing of defaults for routes and handler.
- [0.4]  improve tracking of table "ready" status to no longer use transients.
- [0.8]  support for optimised "show tables".
- [0.9]  always support ?rest_route parameter in API requests.
- [0.10] optimise record updates if the column type is an int.

#### 0.8

- Add base REST API Handler

#### 0.7

- Add Monolog to Composer
- Adjust how we work out the DB "ready" state

#### 0.6

- Provide support CHAR macro type
- Provide support table definitions without `created_at` and `deleted_at` columns
- Ability to directly compare columns within the same table in `WHERE`
- Protect against empty WHEREs for DELETE query.
- Add some support for caching whether a table is "ready" using transients.
- Add "addWhereNotIn()" for query.

#### 0.5.0

Provide support for Foreign Keys in SQL Create

#### 0.4.0

Adjust Insert handler to allow for "IGNORE"

#### 0.3.0

Add support for more MySQL column macrotypes, such a boolean, varchar, text, meta, ip.

#### 0.2.0

Support array-based defintions for table columns and the use of "macro" types to reduce repeating definitions for common
column types.

#### 0.1.0

Start with adding Database Handlers so we can take advantage of Shield's DB Handler.